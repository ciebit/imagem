# Ciebit Imagem

Recursos para trabalho com redimensionamento de imagem e persistência em arquivo.

# Endereços

Código Fonte: https://bitbucket.org/ciebit/imagem/
Packagist: https://packagist.org/packages/ciebit/imagem

# Instruções de Uso

Uma das possiblidades de uso é permite o chamamento das classes através do autoload do Composer

```
#!php

require 'vendor/autoload.php';

$Imagem = new Ciebit\Imagem\Imagem('caminhodaimagen/imagem.jpg');

```

Depois é possível redimensionar e salvar a imagem utilizando apenas dois novos comandos:

```
#!php

require 'vendor/autoload.php';

$Imagem = new Ciebit\Imagem\Imagem('caminhodaimagen/imagem.jpg');

$Imagem->reduzirProporcional(400,300);

$Imagem->salvar('caminho-onde-salvar-a-imagem/');

```

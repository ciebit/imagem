<?php

namespace Ciebit\Imagem;

use SplFileInfo;

class Imagem
{
    private $caminho_completo;
    private $extensao;
    private $imagem;
    private $nome;

    protected $tipos = array(1 => 'gif', 2 => 'jpg', 3 => 'png');

    public function __construct(string $caminho_arquivo)
    {
        $SplFileInfo = new SplFileInfo($caminho_arquivo);
        $this->nome = $SplFileInfo->getFilename();
        $this->extensao = $SplFileInfo->getExtension();
        $this->caminho_completo = $SplFileInfo->getPathname();

        $this->criarImagem();
    }

    public function __destruct()
    {
        $this->fechar();
    }

    public function fechar()
    {
        //Destruindo imagens
        if ($this->imagem) {
            imagedestroy($this->imagem);
        }
    }

    public function adicionarTransparencia($imagem): self
    {
        imagecolortransparent($imagem, imagecolorallocatealpha($imagem, 0, 0, 0, 127));
        imagealphablending($imagem, false);
        imagesavealpha($imagem, true);

        return $this;
    }

    private function criarImagem(): self
    {
        $extensao = $this->obterExtensao();
        $caminho = $this->obterCaminhoCompleto();

        //Criando imagem para ser trabalhada
        switch ($extensao) {
            case "gif":
                $this->imagem = imagecreatefromgif($caminho);
                break;

            case "jpg":
                $this->imagem = imagecreatefromjpeg($caminho);
                break;

            case "png":
                $this->imagem = imagecreatefrompng($caminho);
                break;

            default:
                throw new Exception("Formato de imagem inválido", 1);
                break;
        }

        return $this;
    }

    public function obterAltura(): int
    {
        return imagesy($this->imagem);
    }

    public function obterCaminho(): string
    {
        return $this->caminho;
    }

    public function obterCaminhoCompleto(): string
    {
        return $this->caminho_completo;
    }

    public function obterExtensao(): string
    {
        return $this->extensao;
    }

    public function obterLargura(): int
    {
        return imagesx($this->imagem);
    }

    public function obterNome(): string
    {
        return $this->nome;
    }

    public function obterRecurso()
    {
        return $this->imagem;
    }

    /**
     * Redimensiona imagens proporcionalmente,
     * informando apenas o maximo de largura e o maximo de altura
     */
    public function reduzirProporcional(int $x, int $y): self
    {
        $ret = false;

        $altura = $this->obterAltura();
        $largura = $this->obterLargura();
        $extensao = $this->obterExtensao();

        if ($largura < $x && $altura < $y) {
            return $this;
        }

        if ($largura > $altura) {
            $y = round($x * $altura / $largura);
        } else {
            $x = round($y * $largura / $altura);
        }

        $imagem = imagecreatetruecolor($x, $y);

        if (in_array($this->obterExtensao(), ['gif', 'png'])) {
            $this->adicionarTransparencia($imagem);
        }

        imagecopyresampled(
            $imagem, $this->obterRecurso(),
            0,0,0,0,
            $x, $y,
            $largura, $altura
        );

        $this->imagem = $imagem;

        return $this;
    }

    public function salvar(string $destino = ''): self
    {
        if ($destino == '') {
            $destino = $this->obterCaminhoCompleto();
        } else {
            $destino.= $this->obterNome();
        }


        switch ($this->obterExtensao()) {
            case 'gif':
                imagegif($this->obterRecurso(), $destino);
                break;

            case 'jpg':
                imagejpeg($this->obterRecurso(), $destino);
                break;

            case 'png':
                imagepng($this->obterRecurso(), $destino);
                break;
        }

        return $this;
    }
}
